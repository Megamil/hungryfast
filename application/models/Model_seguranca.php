<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_seguranca extends MY_Model {

	private $usuario;
	private $senha;
	private $id_aplicacao;

	function __construct() {
	    parent::__construct();
	}

	public function validar_login(){

		$this->db->select('*');
		$this->db->from('seg_usuarios');
		$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
		$this->db->where('login_usuario',$this->get_('usuario'));
		$this->db->where('senha_usuario',$this->get_('senha'));
		$login = $this->db->get()->row_array();

		if (isset($login) && $login['ativo_usuario'] && $login['ativo_grupo']) {

			//Histórico de acesso
			$dados = array(

					'fk_usuario' => $login['id_usuario'],
					'ip_usuario_acesso' => $_SERVER['REMOTE_ADDR'],
					'maquina_usuario_acesso' => $_SERVER['HTTP_USER_AGENT'],
					'acesso' => true

				);

			$this->db->insert('seg_log_acesso',$dados);
			$login['id_acesso'] = $this->db->insert_id();

			return $login;
		} else {
			return false;
		}

	}

	public function sair(){

		//Histórico de acesso
		$dados = array(

				'fk_usuario' => $this->session->userdata('usuario'),
				'ip_usuario_acesso' => $_SERVER['REMOTE_ADDR'],
				'maquina_usuario_acesso' => $_SERVER['HTTP_USER_AGENT'],
				'acesso' => false

			);

		return $this->db->insert('seg_log_acesso',$dados);

	}

	public function validar_acesso(){

		$this->db->select(array('link_aplicacao','link_model','titulo_aplicacao'));
		$this->db->from('seg_aplicacao');
		$this->db->join('seg_aplicacoes_grupos','id_aplicacao = fk_aplicacao');
		$this->db->join('seg_controllers','id_controller = fk_controller');
		$this->db->join('seg_models','id_model = fk_model');
		$this->db->where('fk_grupo',$this->session->userdata('grupo'));
		$this->db->where('fk_aplicacao',$this->get_('id_aplicacao'));	

		$link_aplicacao = $this->db->get()->row();								

		if (isset($link_aplicacao)) {
			return array('retorno' => $link_aplicacao, 'acesso' => $this->db->query('SELECT id_log_acesso as 						id_acesso, date_format(data_log_acesso,\'%d/%m/%Y as  %H:%i:%s\') as data_log_acesso
									FROM seg_log_acesso
									where fk_usuario = '.$this->session->userdata('usuario').'
									order by id_log_acesso desc
									limit 1')->row());
		} else {

			return array('retorno' => false,
						'detalhes_acesso' => $this->db->query('select (select descricao_aplicacao 
								from seg_aplicacao
								where id_aplicacao = '.$this->get_('id_aplicacao').') as descricao_aplicacao,

								(select descricao_grupo
								from seg_grupos
								where id_grupo = '.$this->session->userdata('grupo').') as descricao_grupo')->row());

		}

	}

	public function confirmarDados(){

		$id = $this->session->userdata("usuario");
		$grupo = $this->session->userdata("grupo");

		return $this->db->query("select (count(*) = 1) ativo from seg_usuarios inner join seg_grupos on id_grupo = fk_grupo_usuario where ativo_usuario = true and fk_grupo_usuario = {$grupo} and id_usuario = {$id} and ativo_grupo = true")->row()->ativo;

	}

	public function erroPhp($dados = null) {

		$erro = array(
	    				'fk_usuario' => $this->session->userdata('usuario'),
						'erro' => "Linha: {$dados['php_error_line']} - Mensagem: {$dados['php_error_message']}",
						'funcao' => $dados['php_error_filepath'],
						'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
	    			);
	    
	    //Gerando arquivo de erro.
	    log_message('error', 
	    			'Linha: '.$dados['php_error_line'].' Mensagem: "'.$dados['php_error_message'].'" Função: "'.$dados['php_error_filepath'].'"');
	    
	    //Armazenando no banco o log.
	    $this->db->insert('seg_log_erro',$erro);
	    return $this->db->insert_id();
	}

	public function reportarErro($id_log_erro = null,$erro_feedback = null){

		$this->db->where(array('id_log_erro' => $id_log_erro));
		$this->db->update('seg_log_erro',array('erro_feedback' => $erro_feedback));	

		$e = $this->db->error();
		if ($e['code'] != 0) {
			$this->code = $e['code'];
			$this->message = $e['message'];	
			$this->query = $this->db->last_query();
			return $e['message'];		
		} else {
			return true;
		}

	}

	public function logNavegacao($id_aplicacao = null, $where = null, $permissao = null){

		$argumentos = "";

		foreach ($where as $valor) {
			$argumentos .= "/".$valor;
		}

		$dados = array (

			'fk_usuario' => $this->session->userdata('usuario'),
			'fk_aplicacao' => $id_aplicacao,
			'permissao' => $permissao,
			'parametros' => $argumentos

		);

		return $this->db->insert('seg_log_navegacao',$dados);

	}

	public function set_($campo,$valor){
		$this->$campo = $valor;
	}

	public function get_($campo){
		return $this->$campo;
	}

}