<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_pedidos extends MY_Model {
		
		public function view_pedidos(){
			return array('pedidos' => $this->db->query("SELECT 
										id_pedido,
									    nome_usuario,
									    round(preco,2) preco,
									    data,
									    date_format(data,'%d/%m/%Y às %H:%i:%s') as data_formatada,
									    fk_status
									    	from cad_pedidos
									        inner join seg_usuarios on id_usuario = fk_usuario
									        order by data desc")
							->result_array()
						);
		}

		public function badges(){
			return $this->db->query("select count(*) as qtd from cad_pedidos where fk_status = 1")->row()->qtd;
		}

		public function mudarStatus($dados){
			$this->db->where(array('id_pedido' => $dados['id_pedido']));
			$this->db->update('cad_pedidos',$dados);
		}

		public function exibirPedido($id_pedido){

			return $this->db->query("SELECT cad_produtos.fk_usuario AS usuario, 
										       produto, 
										       quantidade, 
										       (SELECT Group_concat(categoria,'-',subcategoria) 
										        FROM   elo_produto_subcategorias 
										               INNER JOIN cad_subcategorias 
										                       ON fk_subcategoria = id_subcategoria 
										               INNER JOIN cad_categorias 
										                       ON fk_categoria = id_categoria 
										        WHERE  fk_produto = id_produto) AS itens 
										FROM   elo_pedido_itens 
										       INNER JOIN cad_produtos 
										               ON id_produto = fk_item 
										       INNER JOIN cad_pedidos 
										               ON id_pedido = fk_pedido 
										WHERE  id_pedido = {$id_pedido}")->result();

		}

	}