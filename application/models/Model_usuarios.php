<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_usuarios extends MY_Model {

		public function view_usuarios(){

			/*[ALTERAR CONFORME PRECISE]*/
			$campos_select = "'id','nome_usuario','email_usuario','login_usuario','ativo','nome_grupo'";
			//As aspas simples não são aceitas como campo, mas para lógica de informar quais campos estão sendo passados "Váriavel campos no array" é preciso.

			$this->db->select(str_replace("'", "", $campos_select));
			$this->db->limit(100);

			/*[ALTERAR CONFORME PRECISE]*/
			return array(

						 'resultado' => 
							$this->db->get("view_lista_usuarios")->result_array(),
						 'usuarios' => 
						 	$this->db->query('SELECT id_usuario, nome_usuario from seg_usuarios')->result(),
						 'grupos' => 
						 	$this->db->query('SELECT id_grupo, nome_grupo from seg_grupos')->result(),
						 'campos' => /*Carrega as informações para montar a lista automaticamente 
						 				(Caso não tenha uma view listar os campos manualmente)*/
						 	$this->db->query("select *, (nome_campo in ({$campos_select})) as selecionado
												from cad_detalhes_views 
												where nome_view = 'view_lista_usuarios' and visivel = true;")->result()
						);
		}

		public function view_novo_usuario(){
			//Lista dos grupos para o select
			return $this->db->get('seg_grupos')->result();

		}

		public function view_editar_usuario($where = null){

			$usuario = $this->db->get_where('seg_usuarios',array('id_usuario' => $where[0]))->row();

			if (isset($usuario)) {
				foreach ($usuario as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			//Lista dos grupos para o select
			return $this->db->get('seg_grupos')->result();

		}

		public function view_editar_perfil(){
			//Lista dos grupos para o select

			$usuario = $this->db->get_where('seg_usuarios',array('id_usuario' => $this->session->userdata('usuario')))->row();

			if (isset($usuario)) {
				foreach ($usuario as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			//Para redirecionar corretamente quando não existem outros conteúdos a serem retornados
			return array('status' => true);

		}

		public function update($valores = null){

			//Alterar
			$tabela = "seg_usuarios";
			$id = 'id_usuario';

			$this->gerarHistorico($id,$tabela,$valores);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_usuarios / update';
				return false;		
			} else {
				return true;
			}

		}

		public function create($valores = null){

			$this->db->insert('seg_usuarios',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_usuarios / create';
				return false;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function senha_Email($senha = null,$login = null) {

			$dados = $this->db->query('select id_usuario as id,email_usuario as email from seg_usuarios where email_usuario =  \''.$login.'\' and ativo_usuario = true;');


			if($dados->num_rows() > 0) {

				$this->db->query('update seg_usuarios set senha_usuario = \''.$senha.'\' where id_usuario = '.$dados->row()->id.';');

				$e = $this->db->error();
				if ($e['code'] != 0) {
					$this->code = $e['code'];
					$this->message = $e['message'];	
					$this->query = $this->db->last_query();
					$this->funcao = 'model_usuarios / senha_Email';

					return false;		
				} else {
					return $dados->row()->email;
				}

			} else {

				return "";

			}
				

		}


	}