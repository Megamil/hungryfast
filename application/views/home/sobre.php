<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo base_url() ?>/home/imagens/shekparts_2_FAF-ICON.png">

    <title>Sobre</title>


    <link href="<?php echo base_url() ?>/home/css/bootstrap.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>/home/css/ie10-viewport-bug-workaround.css" rel="stylesheet">


    <link href="<?php echo base_url() ?>/home/css/starter-template.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/home/css/estilo.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/home/css/carousel.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>/home/css/jquery.bxslider.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>/home/css/hover.css">

    <link href="<?php echo base_url() ?>/home/css/pure.css" rel="stylesheet">



    <script src="<?php echo base_url() ?>/home/js/ie-emulation-modes-warning.js"></script>


    <script src="<?php echo base_url() ?>/home/js/bootstrap.min.js"></script>

    <!-- animacoes teste -->

    <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- fontes externas -->


    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

        @media screen and (min-width: 1200px) and (max-width: 1306px) {.baixo-repre-2{height: 100px;}}
        @media screen and (max-width: 482px) {
            #inp {
                width: 90%;
                position: absolute;
            }
            #inp2 {
                width: 90%;
                position: absolute;
            }
        }

        @media screen and (max-width: 768px) {
            #esquerdo {
                width: 100%;
                float: left;
            }

            #direito {
                width: 100%;
                margin-top: 50px;
            }

            #inp2 {
                width: 80%;
            }
            #inp {
                width: 80%;
            }

            #titulo {
                margin-left: 5%;
                margin-right: 16%;
            }
        }

        @media screen and (min-width: 768px) {
            #esquerdo {
                float: right;
                margin-right: 5%;
                width: 60%;
            }

            #titulo {
                margin-left: 18%;
                margin-right: 16%;
            }
        }


    </style>
</head>


<body style="background-color: white; color: #000;">


<?php
include "menu.php";
?>


<br><br><br><br><br>


<script type="text/javascript" src="<?php echo base_url() ?>/home/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/home/js/jquery.maskedinput.js"></script>



<style>
    @media screen and (min-width: 1921px){
        .input-form{
            width: 60%;
        }
        #corpo_contato{
            margin: 0 auto;width: 1500px;
        }
    }
</style>
<div class="col-lg-12">
    <div id="corpo_contato">







    <br><br>

    <h2 id="titulo" style="color: #000; font-family: 'Open Sans', sans-serif;"><b>Conheça mais sobre o funcionamento da Hungry Fast!</b></h2>
    <br><br>

        <div class="row">

           <div class="col-lg-2"></div>
           <div class="col-lg-4">
               <img src="<?php echo base_url() ?>/home/imagens_tcc/celular.jpg" alt="" class="img img-responsive img-rounded" style="border: 2px solid black">
           </div>
           <div class="col-lg-4">
               <h3 style="width: 100%; text-align: center">Acesso ao aplicativo</h3>
               <p>Baixe o aplicativo na plataforma App Store e faça já o cadastro dentro de nossa plataforma, caso ainda não tenha
                   cadastro, de maneira simples através de login no facebook.
                   </p>
           </div>
           <div class="col-lg-2"></div>


        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%;">Monte seu lanche</h3>
                <p>Com o aplicativo HungryFast, você é capaz de montar seu lanche em poucos minutos, evitando filas. Ainda há a possíbilidade
                de pagamento com o cartão de crédito, para a comodidade de nossos clientes.</p>
            </div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/ingredientes.jpg" alt="" class="img img-responsive img-rounded" style="border: 2px solid black">
            </div>
            <div class="col-lg-2"></div>

        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/confirmar.png" alt="" class="img img-rounded img-responsive" style="border: 2px solid black; ">
            </div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%">Confirme o seu pedido</h3>
                <p>
                    Confirme seu pedido de forma rápida e prática pelo aplicativo,
                    sem mesmo ter a necessidade de estar dentro do restaurante.
                </p>
            </div>
            <div class="col-lg-2"></div>

        </div>

        <div class="row" style="margin-top: 10%">

            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <h3 style="text-align: center; width: 100%">Retire seu pedido</h3>
                <p>
                    Após todos os passos concluidos e o pagamento realizado, dentro de alguns
                    minutos seu lanche estará pronto para ser retirado no restaurante.
                </p>
            </div>
            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>/home/imagens_tcc/retirar.jpg" alt="" class="img img-rounded img-responsive" style="border: 2px solid black">
            </div>
            <div class="col-lg-2"></div>

        </div>

</div>
</div>

<div style="padding-bottom: 100px"></div>
<div class="baixo-repre-2">
</div>

</body>
<p>&nbsp;</p>
<?php
include "rodape.php";
?>
