<?php defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . '/libraries/REST_Controller.php';
	use Restserver\Libraries\REST_Controller;

	class Controller_webservice extends REST_Controller  {

		private $aparelho 	= null;
		private $ip 		= null;
		private $sistema 	= null;

		function __construct() {
			
			parent::__construct();
			// Ex. definindo limites, habilitar em application/config/rest.php
			$this->methods['perfil_get']['limit'] = 500; // 500 requisições por hora, usuário ou chave
			$this->methods['users_post']['limit'] = 100; // 100 requisições por hora, usuário ou chave
			$this->load->model('model_webservice');
			$this->load->model('model_produtos');
			$this->load->model('model_usuarios');

			//Resgatando dados do acesso.
			$this->ip 		= $_SERVER['REMOTE_ADDR'];
			$this->sistema 	= $_SERVER['HTTP_USER_AGENT'];
			$iPod    		= stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
			$iPhone  		= stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
			$iPad    		= stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
			$Android 		= stripos($_SERVER['HTTP_USER_AGENT'],"Android");
			$webOS   		= stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

			if($iPod || $iPhone || $iPad){
			    $this->aparelho = "Apple";
			} else if($Android){
				$this->aparelho = "Google";
			} else { //Caso abra pelo Navegador de MAC / PC / Linux
				$this->aparelho = "Desktop";
			}

		}

		##################################################################
		/**
		*	Garante que quem está usando o sistema possúi credenciais de acesso.
		*	@param $grupo_permitir ao passar um valor será limitado o acesso para o grupo em questão,
		* 	@param $valores array que rece o login_usuario e senha_usuario (em sha1) para validar o acesso.
		*/
		public function autenticacao($grupo_permitir = null){

			$PHP_AUTH_USER = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : null;
            $PHP_AUTH_PW = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : null;

            $valores = array(
                'email_usuario' => $PHP_AUTH_USER,
                'senha_usuario' => $PHP_AUTH_PW
            );

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('email_usuario','Login do Usuário','required');
			$this->form_validation->set_rules('senha_usuario','Senha do Usuário (Criptografada)','required');

			if ($this->form_validation->run()) {

				$autenticacao = $this->model_webservice->validar_login($valores);
				
				if($autenticacao) {

					if(isset($grupo_permitir) && $grupo_permitir != $autenticacao->fk_grupo_usuario){
						$this->response(array('status' => STATUS_FALHA_PERFIL, 'resultado' => 'Sem permissão para chamar essa função'),Self::HTTP_NOT_FOUND); //404
					} else if(!$autenticacao->ativo_usuario) {
						$this->response(array('status' => STATUS_FALHA_PERFIL_INATIVO, 'resultado' => 'Perfil Inativo!'),Self::HTTP_NOT_FOUND); //404
					} else {
						return $autenticacao;
					}

				} else {
					$this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos!'),Self::HTTP_NOT_FOUND); //404
				}

			} else { //Campos em branco.				

				$erros = strip_tags(validation_errors());
				$this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

			}

		}

		##################################################################
		/** 
		*	WS1 Recuperar Senha
		* 	@param $valores array que recebe o email_usuario para localizar o E-mail e criar uma nova senha
		*/
		public function esqueci_senha_get(){

			$valores = array('email_usuario' => $this->get('email_usuario'));

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('email_usuario','E-mail do Usuário','required');

  			if ($this->form_validation->run()) {

				$this->load->helper('string');
				$senha = random_string('alnum',10);

				$email = $this->model_usuarios->senha_Email(sha1($senha),$this->get('email_usuario'));

				//Usuário inativo ou desativado
				if($email == ""){

					$this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos ou perfil inativo!'),Self::HTTP_NOT_FOUND); //404

				} else { //Senha enviada para o E-mail.

					// Detalhes do Email. 
					$this->email->from('noreply@hungryfast.net', 'Hungry Fast | Troca de senha'); 
					$this->email->to($email); 
					$this->email->subject('Hungry Fast | Troca de senha'); 
					$this->email->message('<h1>
											<a href="http://'.$_SERVER['HTTP_HOST'].base_url().'">
												Hungry Fast
											</a>
										   </h1> 
									Recebemos sua solicitação de nova senha. <br> 
									Sua nova senha agora é: <strong>'.$senha.'</strong><br>
									<hr>
									<small>Solicitado pelo IP: '.$_SERVER['REMOTE_ADDR'].'</small> <br>
									<small>Sistema / Navegador: '.$_SERVER['HTTP_USER_AGENT'].'</small> <br>
									Em: '.date('d/m/Y H:i:s').'<br>
									<img src="http://'.$_SERVER['HTTP_HOST'].base_url().'/style/img/rodape.png" alt="Rodapé">');

					// Enviar... 
					if ($this->email->send()) { 

						$this->response(array('status' => STATUS_OK, 'resultado' => "Nova Senha enviada para o E-mail: (".$email.")"),Self::HTTP_OK); //200

					} else {

						$this->response(array('status' => STATUS_FALHA, 'resultado' => "Erro ao enviar senha: ".$this->email->print_debugger()),Self::HTTP_BAD_REQUEST); //400

					}

				}


			} else { //Campos Preenchidos

				$erros = strip_tags(validation_errors());
				$this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

			}  

		}
		
		##################################################################
		/** 
		*	WS2 Login
		* 	@param $autenticacao array que recebe os dados de acesso do usuário
		*/	
		public function login_usuario_post(){

			$autenticacao = $this->autenticacao();
			if($autenticacao){
				$this->response(array('status' => STATUS_OK, 'resultado' => "Login realizado com sucesso", 'dados' => $autenticacao),Self::HTTP_OK); //200
			}

		}

		##################################################################
		/** 
		*	WS3 Criar Usuário
		* 	@param $valores array que recebe os dados de acesso do usuário a ser criado
		*/
		public function criar_usuario_post(){

			$valores = array(
				'nome_usuario'		=> $this->post('nome_usuario'),
				'login_usuario' 	=> $this->post('email_usuario'),
				'email_usuario' 	=> $this->post('email_usuario'),
				'senha_usuario' 	=> $this->post('senha_usuario'),
				'ativo_usuario'		=> 1,
				'fk_grupo_usuario' 	=> 3,
			);

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('nome_usuario' ,'Nome do Usuário',				 'required');
			$this->form_validation->set_rules('email_usuario','Email do Usuário',				 'required|is_unique[seg_usuarios.email_usuario]');
			$this->form_validation->set_rules('senha_usuario','Senha do Usuário (Criptografada)','required');

  			if ($this->form_validation->run()) {

				$this->model_webservice->start();
				$id = $this->model_webservice->criarUsuario($valores);
				$commit = $this->model_webservice->commit();

				$this->base64ToImage($id,$this->post('cliente'),"cliente","cliente");

				if ($commit['status']){

					$this->response(array('status' => STATUS_OK, 'resultado' => "Perfil criado com sucesso."),Self::HTTP_OK); //200	  				

				} else {

					$this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"),Self::HTTP_NOT_FOUND); //404
				}

			} else { //Campos Preenchidos

				$erros = strip_tags(validation_errors());
				$this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

			} 

		}

		##################################################################
		/** 
		*	WS4 Listar Usuário
		*/
		public function listar_usuario_get(){
			$autenticacao = $this->autenticacao();
			if($autenticacao){
				$this->response(array('status' => STATUS_OK, 'resultado' => "Lista de usuários", 'usuarios' => $this->model_webservice->listarUsuarios()),Self::HTTP_OK); //200
			}

		}

		######################################################
		//Pré editar
		######################################################
	    public function pre_editar_get(){

	        $autenticacao = $this->autenticacao();
	        if($autenticacao){
	            $this->response(array('status'    => STATUS_OK,
	                'resultado' => "dados do usuário",
	                'dados' => $autenticacao),Self::HTTP_OK); //200
	        }

	    }

		##################################################################
		/** 
		*	WS5 Editar Usuário
		* 	@param $autenticacao array que recebe os dados de acesso do usuário
		*/
		public function editar_usuario_post(){

			$autenticacao = $this->autenticacao();
			if($autenticacao){

				$valores = array(
					'id_usuario'	=> $autenticacao->id_usuario,
					'nome_usuario' 	=> $this->post('nome_usuario'),
					'login_usuario' => $this->post('email_usuario'),
					'email_usuario' => $this->post('email_usuario'),
					'senha_usuario' => $this->post('senha_usuario')
				);

				if(!isset($valores['nome_usuario'])){
					unset($valores['nome_usuario']);
				}

				if(!isset($valores['email_usuario'])){
					unset($valores['email_usuario']);
					unset($valores['login_usuario']);
				}

				if(!isset($valores['senha_usuario'])){
					unset($valores['senha_usuario']);
				}

				$this->form_validation->set_data($valores);
				$this->form_validation->set_rules('nome_usuario','Nome do Usuário','max_length[100]');

				if(isset($valores['email_usuario']) && $valores['email_usuario'] != $autenticacao->email_usuario){	
					$this->form_validation->set_rules('email_usuario','Email do Usuário','required|is_unique[seg_usuarios.email_usuario]');
				}

	  			if ($this->form_validation->run()) {

					$this->model_webservice->start();
					$this->model_webservice->editarUsuario($valores);
					$commit = $this->model_webservice->commit();

					$this->base64ToImage($valores['id_usuario'],$this->post('cliente'),"cliente","cliente");

					if ($commit['status']){

						$this->response(array('status' => STATUS_OK, 'resultado' => "Perfil {$autenticacao->nome_usuario} editado com sucesso."),Self::HTTP_OK); //200	  				

					} else {

						$this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"),Self::HTTP_NOT_FOUND); //404
					}

				} else { //Campos Preenchidos

					$erros = strip_tags(validation_errors());
					$this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

				} 

			} 

		}

	######################################################	
	//Cadastrar forma de pagamento
	######################################################	
    public function nova_forma_pagamento_post() {

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $valores = array(
                'nome_cartao'               => $this->post('nome_cartao'),
                'numero_cartao'             => $this->post('numero_cartao'),
                'data_vencimento_cartao'    => $this->post('data_vencimento_cartao'),
                'cvv_cartao'                => $this->post('cvv_cartao'),
                'data_nascimento'           => $this->post('data_nascimento')
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('nome_cartao',		    'Nome cartão','required');
            $this->form_validation->set_rules('numero_cartao',		    'Número cartão','required|callback_validar_num_cartao['.$autenticacao->id_usuario.']');
            $this->form_validation->set_rules('data_vencimento_cartao', 'Data vencimento','required');
            $this->form_validation->set_rules('cvv_cartao',			    'CVV cartão','required');
            $this->form_validation->set_rules('data_nascimento',        'Data nascimento','required');

            $valores['fk_usuario'] = $autenticacao->id_usuario;

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $this->model_webservice->novaFormaPagamento($valores);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK,
                        'resultado' => "Dados cadastrados com sucesso"
                    ),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao inserir.",
                        'log' => $commit),Self::HTTP_OK);
                }

            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    public function validar_num_cartao($cartao,$id){

        $consulta = $this->model_webservice->validarNumCartao($cartao,$id);

        if (isset($consulta) && $consulta->num_rows() == 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('validar_num_cartao', 'O cartão deve ser único por usuário');
            return FALSE;
        }

    }

    ######################################################
	//Listar Formas de pagamento
	######################################################
    public function listar_formas_pagamento_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $dados = $this->model_webservice->listarFormasPagamentos($autenticacao->id_usuario);
            $this->response(array('status'    => STATUS_OK,
                'resultado' => "Listas das formas de pagamento:",
                'formas' => $dados),Self::HTTP_OK); //200
        }

    }

    ######################################################
	//Editar forma de Pagamento
	######################################################
    public function editar_forma_pagamento_post() {

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $valores = array(
                'id_pagamento'              => $this->post('id_pagamento'),
                'nome_cartao'               => $this->post('nome_cartao'),
                'numero_cartao'             => $this->post('numero_cartao'),
                'data_vencimento_cartao'    => $this->post('data_vencimento_cartao'),
                'cvv_cartao'                => $this->post('cvv_cartao'),
                'data_nascimento'           => $this->post('data_nascimento')
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('id_pagamento',		   'ID da forma de pagamento','required');
            $this->form_validation->set_rules('nome_cartao',		   'Nome cartão','required');
            $this->form_validation->set_rules('numero_cartao',		   'Número cartão','required|callback_validar_num_cartao['.$autenticacao->id_usuario.']');
            $this->form_validation->set_rules('data_vencimento_cartao','Data vencimento','required');
            $this->form_validation->set_rules('cvv_cartao',			   'CVV cartão','required');
            $this->form_validation->set_rules('data_nascimento',        'Data nascimento','required');

            $valores['fk_usuario'] = $autenticacao->id_usuario;

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $this->model_webservice->editarFormaPagamento($valores);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK,
                        'resultado' => "Dados editados com sucesso"
                    ),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao editar.",
                        'log' => $commit),Self::HTTP_OK);
                }

            } else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
	//Listar Produtos
	######################################################
    public function listar_produtos_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $produtos = $this->model_webservice->listarProdutos($autenticacao->id_usuario);
            $this->response(array('status'    => STATUS_OK,
                'resultado' => "Listas de produtos:",
                'produtos' => $produtos),Self::HTTP_OK); //200
        }

    }

    ######################################################
	//Detalhes Produto
	######################################################
    public function detalhes_produto_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $valores = array(
                'fk_produto' => $this->get('fk_produto')
            );
			
			$this->form_validation->set_data($valores);
            $this->form_validation->set_rules('fk_produto', 'ID do produto','required');
			
			if ($this->form_validation->run()) {
			
				$dados = $this->model_webservice->listarDetalhesProduto($valores);
	            $this->response(array('status'    => STATUS_OK,
	                'resultado' => "Detalhes do produto:",
	                'produtos' => $dados['produto'],
	                'itens' => $dados['itens']),Self::HTTP_OK); //200

			} else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
	//Listar Categorias
	######################################################
    public function listar_categorias_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $categorias = $this->model_webservice->listarCategorias();
            $this->response(array('status'    => STATUS_OK,
                'resultado' => "Listas de categorias:",
                'categorias' => $categorias),Self::HTTP_OK); //200
        }

    }

    ######################################################
	//Listar SubCategorias
	######################################################
    public function listar_sub_categorias_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $valores = array(
                'fk_categoria' => $this->get('fk_categoria')
            );
			
			$this->form_validation->set_data($valores);
            $this->form_validation->set_rules('fk_categoria', 'ID da categoria','required');
			
			if ($this->form_validation->run()) {
			
				$subcategorias = $this->model_webservice->listarSubCategorias($valores['fk_categoria']);
	            
	            $this->response(array('status'    => STATUS_OK,
	                'resultado' => "Listas de SubCategorias:",
	                'subcategorias' => $subcategorias),Self::HTTP_OK); //200

			} else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }

    ######################################################
	//Cadastro de lanche
	######################################################
    public function cadastrar_lanche_post(){

    	$autenticacao = $this->autenticacao();
        if($autenticacao){

	    	$dados = array (
				'produto' 	=> $this->post('produto'),
				'preco' 	=> $this->post('preco'),
				'fk_usuario' => $autenticacao->id_usuario
			);

	    	$this->form_validation->set_data($dados);
	    	$this->form_validation->set_rules('produto','Produto','required');
			$this->form_validation->set_rules('preco','Preço','required');

			if ($this->form_validation->run()) {

				$this->model_produtos->start();
				
				$id = $this->model_produtos->add_produtos($dados);

				$commit = $this->model_produtos->commit();

				if ($commit['status']) {

					$this->base64ToImage($id,$this->post('imagem'),"produto","produto");
					
					$itens = $this->post('item');
					
					foreach ($itens as $value) {
						$this->model_produtos->inserirItens($id,$value);
					}

					$this->response(array('status'    => STATUS_OK,
	                'resultado' => "Produto cadastrado com sucesso",
	                'id' => $id),Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao cadastrar.",
                        'log' => $commit),Self::HTTP_OK);
				}


			} else {

				 $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

			}
		}
    }

    ######################################################
	//Cadastro de lanche
	######################################################
    public function cadastrar_pedido_post(){

    	$autenticacao = $this->autenticacao();
        if($autenticacao){

	    	$dados = array (
				'preco' 		=> $this->post('preco'),
				'fk_pagamento' 	=> $this->post('fk_pagamento'),
				'fk_usuario' 	=> $autenticacao->id_usuario,
				'fk_status' 	=> 1
			);

	    	$this->form_validation->set_data($dados);
	    	$this->form_validation->set_rules('preco','Preço','required');
	    	$this->form_validation->set_rules('fk_pagamento','Forma Pagamento','required');

			if ($this->form_validation->run()) {

				$this->model_webservice->start();
				
				$id = $this->model_webservice->addPedido($dados);

				$commit = $this->model_webservice->commit();

				if ($commit['status']) {

					$itens = $this->post('item');
					$quantidade = $this->post('quantidade');
					
					foreach ($itens as $key => $value) {
						$this->model_webservice->inserirItensPedido($id,$value,$quantidade[$key]);
					}

					$this->response(array('status'    => STATUS_OK,
	                'resultado' => "Pedido cadastrado com sucesso",
	                'id' => $id),Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => STATUS_FALHA,
                        'resultado' => "Falha ao cadastrar.",
                        'log' => $commit),Self::HTTP_OK);
				}


			} else {

				$erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

			}
		}
    }

    ######################################################
	//Listar Categorias
	######################################################
    public function listar_pedidos_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $pedidos = $this->model_webservice->listarPedidos($autenticacao->id_usuario);
            $this->response(array('status'    => STATUS_OK,
                'resultado' => "Listas de pedidos:",
                'pedidos' => $pedidos),Self::HTTP_OK); //200
        }

    }

    ######################################################
	//Listar SubCategorias
	######################################################
    public function listar_detalhes_pedido_get(){

        $autenticacao = $this->autenticacao();
        if($autenticacao){

            $valores = array(
                'fk_pedido' => $this->get('fk_pedido')
            );
			
			$this->form_validation->set_data($valores);
            $this->form_validation->set_rules('fk_pedido', 'ID do pedido','required');
			
			if ($this->form_validation->run()) {
			
				$pedido = $this->model_webservice->listardetalhesPedidos($valores['fk_pedido']);
	            
	            $this->response(array('status'    => STATUS_OK,
	                'resultado' => "Detalhes do pedido:",
	                'pedido' => $pedido['pedido'],
	                'itens' => $pedido['itens']),Self::HTTP_OK); //200

			} else { //Campos Preenchidos

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_OK); //400

            }

        }

    }



	    ##################################################################
		/** 
		*	Busca por CEP
		* 	@param $cep recebe o CEP para buscar o endereço completo
		*/
	    public function buscar_cep_get(){

	        $valores = array('cep'	=> $this->get('cep'));

	        $this->form_validation->set_data($valores);
	        $this->form_validation->set_rules('cep','CEP','required');

	        if ($this->form_validation->run()) {

	            $curl = curl_init();

	            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://viacep.com.br/ws/{$valores['cep']}/json/unicode",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: Application/json"
                ),
	            ));

	            $response = curl_exec($curl);
	            $err = curl_error($curl);

	            curl_close($curl);
	            $dado = json_decode($response);
	            $dados['cep']  		  = !isset($dado->cep) 		   ? "" : $dado->cep;
	            $dados['logradouro']  = !isset($dado->logradouro)  ? "" : $dado->logradouro;
	            $dados['complemento'] = !isset($dado->complemento) ? "" : $dado->complemento;
	            $dados['bairro']      = !isset($dado->bairro) 	   ? "" : $dado->bairro;
	            $dados['localidade']  = !isset($dado->localidade)  ? "" : $dado->localidade;
	            $dados['uf']          = !isset($dado->uf) 		   ? "" : $dado->uf;
	            //Caso queria trazer o ID da UF de uma tabela relacional
	            //$dados['uf_id']          = !isset($dado->uf) 	   ? "" : $this->model_webservice->filtroUf($dado->uf);

	            if ($err) {

	                $this->response(array('status' => STATUS_FALHA, 'resultado' => $err),Self::HTTP_BAD_REQUEST); //400

	            } else {

	                $this->response(array('status' => STATUS_OK, 'resultado' => "Retorno do via cep.", "endereco" => $dados),Self::HTTP_OK); //200

	            }

	        } else {

	            $erros = strip_tags(validation_errors());
	            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

	        }

	    }

	    ##################################################################
		/** 
		*	Formata a data para o banco de dados
		* 	@param $data 		recebe a data que deseja converter
		* 	@param $timestamp 	recebe um booleano, TRUE caso seja timestamp.
		*/
	    public function data($data = null,$timestamp = null){

			if (isset($data) && $data != "" && $timestamp) {

				return date("Y-m-d H:i:s",strtotime(str_replace('/','-',$data)));
				
			} else if(isset($data) && $data != "" && !$timestamp) {

				return date("Y-m-d",strtotime(str_replace('/','-',$data)));

			} else {

				return 0;

			}

		}

	    ##################################################################
		/** 
		*	Formata moeda
		* 	@method formatMoeda 		transforma o valor deixando com padrão Brasileiro
		* 	@method formatMoedaBanco 	transforma o valor deixando com padrão para o banco de dados
		* 	@param $valor 				valor a ser convertido
		*/
	    public function formatMoeda($valor){
	        return 'R$ '.number_format($valor, 2, ',', '.'); // R$ 1.000,00
	    }
		
		public function formatMoedaBanco($valor){
	        return number_format(preg_replace('/[^0-9]/', '', $valor), 2, '.', ''); // 1000.00
	    }

	    ##################################################################
		/** 
		*	Remove mascaras, deixando somente números e letras
		* 	@param $valor	valor a ser convertido
		*/
	    public function removerMascaras($valor) {
	        return preg_replace('/[^0-9a-zA-Z]/', '', $valor);
	    }
		
	   	##################################################################
		/** 
		*	Upload de imagens em Base64 para .PNG e criando uma pasta por usuário. 
		* 	@param $id
		* 	@param $imagem
		* 	@param $nome
		* 	@param $perfil
		*/
	    function base64ToImage($id,$imagem,$nome = '',$perfil){

	        if (!is_null($imagem) && $imagem != '') {

	            $imagem = str_replace("\n", "", $imagem);

	            $imagem = str_replace("\r", "", $imagem);

	            $imagem = str_replace("\\n", "", $imagem);

	            $imagem = str_replace("\\r", "", $imagem);

	            $imagem = str_replace(" ", "+", $imagem);

	            $fileName = $nome.'.png';
	            $imagem = base64_decode($imagem);
	            $endereco = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/'.$perfil.'s/'.$perfil.'_'.$id;

	            if (!file_exists($endereco)) {
	                mkdir($endereco, 0777, true);
	            }

	            if(file_exists($endereco.'/'.$fileName)) {

	                unlink($endereco.'/'.$fileName);

	            }

	            return file_put_contents($endereco.'/'.$fileName, $imagem);
	        }

	        return true;
	    }
	
	}

?>