<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_produtos extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_produtos');
		    
	}

	/*
		Categorias
	*/
	public function criar_categoria(){

		$this->form_validation->set_rules('categoria','Categoria','required');

		$dados = array (
					'categoria' => $this->input->post('categoria')
				);

		if ($this->form_validation->run()) {

			$this->model_produtos->start();
			
			$this->model_produtos->add_categorias($dados);

			$commit = $this->model_produtos->commit();

			if ($commit['status']) {
				$this->aviso('Registro Criado','Categora "'.$dados['categoria'].'" criada com sucesso!.','success',false);

				redirect('main/redirecionar/15');
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/15');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/15');

		}

	}

	public function editar_categoria(){

		$this->form_validation->set_rules('id_categoria','ID categoria','required');
		$this->form_validation->set_rules('categoria','categoria','required');

		$dados = array (
					'id_categoria' => $this->input->post('id_categoria'),
					'categoria' => $this->input->post('categoria')
				);

		if ($this->form_validation->run()) {

			$this->model_produtos->start();
			
			$this->model_produtos->editar_categorias($dados);

			$commit = $this->model_produtos->commit();

			if ($commit['status']) {
				$this->aviso('Registro Criado','Categora "'.$dados['categoria'].'" editada com sucesso!.','success',false);

				redirect('main/redirecionar/15');
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao editar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/15');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/15');

		}

	}

	public function exibir_categoria(){
		
		$dados = $this->model_produtos->exibircategoria($this->uri->segment(3));

		echo '<div class="modal-body">
        <div class="row">
      		<div class="col-md-1">
      			<label>ID</label>
      			<input type="hidden" class="form-control" name="id_categoria" value="'.$dados->id_categoria.'">
      			'.$dados->id_categoria.'
      		</div>
      		<div class="col-md-11">
      			<label>Categoria</label>
      			<input type="text" class="form-control" name="categoria" placeholder="Categoria" value="'.$dados->categoria.'">
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
      </div>';

	}


	/*
		SubCategorias
	*/
	public function criar_subcategoria(){

		$this->form_validation->set_rules('fk_categoria','FK Categoria','required');
		$this->form_validation->set_rules('subcategoria','Subcategoria','required');
		$this->form_validation->set_rules('preco','Preço','required');

		$dados = array (
			'fk_categoria' 	=> $this->input->post('fk_categoria'),
			'subcategoria' 	=> $this->input->post('subcategoria'),
			'preco' 		=> $this->input->post('preco')
		);

		if ($this->form_validation->run()) {

			$dados['preco'] = $this->formatMoedaBanco($dados['preco']);

			$this->model_produtos->start();
			
			$this->model_produtos->add_subcategorias($dados);

			$commit = $this->model_produtos->commit();

			if ($commit['status']) {
				$this->aviso('Registro Criado','Subcategora "'.$dados['subcategoria'].'" criada com sucesso!.','success',false);

				redirect('main/redirecionar/16');
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/16');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/16');

		}

	}

	public function editar_subcategoria(){

		$this->form_validation->set_rules('id_subcategoria','ID Subcategoria','required');
		$this->form_validation->set_rules('fk_categoria','FK Categoria','required');
		$this->form_validation->set_rules('subcategoria','Subcategoria','required');
		$this->form_validation->set_rules('preco','Preço','required');

		$dados = array (
			'id_subcategoria' 	=> $this->input->post('id_subcategoria'),
			'fk_categoria' 		=> $this->input->post('fk_categoria'),
			'subcategoria' 		=> $this->input->post('subcategoria'),
			'preco' 			=> $this->input->post('preco')
		);

		if ($this->form_validation->run()) {

			$dados['preco'] = $this->formatMoedaBanco($dados['preco']);

			$this->model_produtos->start();
			
			$this->model_produtos->editar_subcategorias($dados);

			$commit = $this->model_produtos->commit();

			if ($commit['status']) {
				$this->aviso('Registro Criado','Subcategora "'.$dados['subcategoria'].'" editada com sucesso!.','success',false);

				redirect('main/redirecionar/16');
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao editar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/16');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/16');

		}

	}

	public function exibir_subcategoria(){
		
		$dados = $this->model_produtos->exibirSubcategoria($this->uri->segment(3));

		echo form_open('Controller_produtos/editar_subcategoria'); 
		echo '<div class="modal-body">
        <div class="row">
      		<div class="col-md-1">
      			<label>ID</label>
      			<input type="hidden" class="form-control" name="id_subcategoria" value="'.$dados['subcategoria']->id_subcategoria.'">
      			'.$dados['subcategoria']->id_subcategoria.'
      		</div>
      		<div class="col-md-4">
      			<label>Categoria</label>
      			<select name="fk_categoria" style="width:100%;">';
      				
      				foreach ($dados['categorias'] as $chave => $categoria) {
      					if($dados['subcategoria']->fk_categoria == $categoria->id_categoria) {
							echo "<option value=\"{$categoria->id_categoria}\" selected>{$categoria->categoria}</option>";
      					} else {
      						echo "<option value=\"{$categoria->id_categoria}\">{$categoria->categoria}</option>";
      					}
      				}
      				
      			echo '</select>
      		</div>

      		<div class="col-md-4">
      			<label>SubCategoria</label>
      			<input type="text" class="form-control" name="subcategoria" placeholder="SubCategoria" value="'.$dados['subcategoria']->subcategoria.'">
      		</div>

      		<div class="col-md-3">
      			<label>Preço</label>
      			<input type="text" class="form-control mascara_monetaria" name="preco" placeholder="Preço" value="'.$dados['subcategoria']->preco.'">
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary" id="validar_Enviar">Salvar</button>
      </div>';

		echo form_close(); 

	}


	/*
		Produtos
	*/
	public function criar_produto(){

		$this->form_validation->set_rules('produto','Produto','required');
		$this->form_validation->set_rules('calorias','Calorias','required');
		$this->form_validation->set_rules('preco','Preço','required');
		$this->form_validation->set_rules('descricao','Descrição','required');

		$dados = array (
			'produto' 	=> $this->input->post('produto'),
			'calorias' 	=> $this->input->post('calorias'),
			'preco' 	=> $this->input->post('preco'),
			'descricao' => $this->input->post('descricao')
		);

		if ($this->form_validation->run()) {

			$dados['preco'] = $this->formatMoedaBanco($dados['preco']);

			$this->model_produtos->start();
			
			$id = $this->model_produtos->add_produtos($dados);

			$commit = $this->model_produtos->commit();

			if ($commit['status']) {

				$this->armazenarImagem($id,$_FILES['imagem']['name']);
				$itens = $this->input->post('item');
				
				foreach ($itens as $value) {
					$this->model_produtos->inserirItens($id,$value);
				}

				$this->aviso('Registro Criado','Produto "'.$dados['produto'].'" por  R$ "'.$dados['preco'].'" criada com sucesso!.','success',false);

				redirect('main/redirecionar/17');
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/17');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/17');

		}

	}

	public function exibir_produto(){
		
		$dados = $this->model_produtos->exibirProduto($this->uri->segment(3));

		echo '<div class="modal-body">';
        echo form_open_multipart('Controller_produtos/edita_produto',array('id'=>'formImageEdit')); 
      	echo '<div class="row">
      		<div class="col-md-3">
  				
  				<img src="'.base_url().'upload/produtos/produto_'.$dados['produto']['id_produto'].'/produto.png" id="fotoEdit" width="80px" style="margin-bottom: 10px;margin-left: 20px;border:none" onerror="this.src=\''.base_url().'style/img/favicon.ico\';">
	            <p style="width: 10px"> 
	            	<input type="file" id="imagem" name="imagem" width="100"  style="margin-bottom: 20px" onchange="readURL(this,\'fotoEdit\');" />
	            </p>

      		</div>
      		<div class="col-md-3">
      			<label>Produto</label>
      			<input type="hidden" name="id_produto" value="'.$dados['produto']['id_produto'].'">
      			<input type="text" class="form-control obrigatorio" name="produto" placeholder="Produto" value="'.$dados['produto']['produto'].'">
      		</div>
      		<div class="col-md-3">
      			<label>Calorias</label>
      			<input type="text" class="form-control validar_numeros obrigatorio" name="calorias" placeholder="Calorias" value="'.$dados['produto']['calorias'].'">
      		</div>
      		<div class="col-md-3">
      			<label>Preço</label>
      			<input type="text" class="form-control mascara_monetaria obrigatorio" name="preco" placeholder="Preço" value="'.$dados['produto']['preco'].'">
      		</div>
      	</div>

      	<div class="row">
      		<div class="col-md-12">
      			<label>Descrição</label>
      			<textarea name="descricao" style="width: 100%; height: 100px;">'.$dados['produto']['descricao'].'</textarea>
      		</div>
      	</div>

      	<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-success" style="width:100%;" id="addItemEdit"><i class="glyphicon glyphicon-plus-sign"></i>Adicionar</button>
			</div>
		</div>

      	<div class="row" id="itemEdit">';

			 foreach ($dados['elo'] as $key => $value) {
			 	echo '<span>
			 		<div class="col-md-10">
		      			<label>Contém:</label>
		      			<select style="width: 100%;" name="item[]">';

			 	foreach ($dados['categorias'] as $chave => $valor) {
			 		if($valor->id == $value['fk_subcategoria']) {
			 			echo '<option value="'.$valor->id.'" selected>'.$valor->opcao.'</option>';
			 		} else {
			 			echo '<option value="'.$valor->id.'">'.$valor->opcao.'</option>';
			 		}
			 	}

			 	echo '</select>
			 	</div>
			 	<div class="col-md-2">
			 	<button type="button" class="btn btn-danger remover" style="margin-top:22px">
			 		<i class="glyphicon glyphicon-trash"></i>
			 	</button>
			 	</div>
			 	</span>';

			} 

      		echo '</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary enviarEdicao" id="validar_Enviar">Editar</button>
      </div>';
      echo form_close();

	}

	public function edita_produto(){

		$this->form_validation->set_rules('id_produto','ID do Produto','required');
		$this->form_validation->set_rules('produto','Produto','required');
		$this->form_validation->set_rules('calorias','Calorias','required');
		$this->form_validation->set_rules('preco','Preço','required');
		$this->form_validation->set_rules('descricao','Descrição','required');

		$dados = array (
			'id_produto' => $this->input->post('id_produto'),
			'produto' 	 => $this->input->post('produto'),
			'calorias' 	 => $this->input->post('calorias'),
			'preco' 	 => $this->input->post('preco'),
			'descricao'  => $this->input->post('descricao')
		);

		if ($this->form_validation->run()) {

			$dados['preco'] = $this->formatMoedaBanco($dados['preco']);

			$this->model_produtos->start();
			
			$this->model_produtos->editar_produtos($dados);

			$commit = $this->model_produtos->commit();

			if ($commit['status']) {

				$this->armazenarImagem($dados['id_produto'],$_FILES['imagem']['name']);

				$this->model_produtos->limparItens($dados['id_produto']);

				$itens = $this->input->post('item');
				
				foreach ($itens as $value) {
					$this->model_produtos->inserirItens($dados['id_produto'],$value);
				}

				$this->aviso('Registro Criado','Produto "'.$dados['produto'].'" por  R$ "'.$dados['preco'].'" editado com sucesso!.','success',false);

				redirect('main/redirecionar/17');
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao editar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/17');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/17');

		}

	}

	//Armazena imagem do cliente, se for do facebook não é chamada está função.
	public function armazenarImagem($id = null, $imagem = null){
		if(isset($imagem) && $imagem != ''){

			$config['upload_path']   = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/produtos/produto_'.$id;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['file_name']     = 'produto.png';

			 if (!file_exists($config['upload_path'])) {
                mkdir($config['upload_path'], 0777, true);
            }

			if(file_exists($config['upload_path'].'/'.$config['file_name'])){
		 
		    	unlink($config['upload_path'].'/'.$config['file_name']);
		    
			}
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('imagem');

		}

	}

	public function formatMoedaBanco($valor){
		
		$valor = str_replace(',', '.', $valor);
        return number_format(preg_replace('/[^0-9.]*/', '', $valor), 2, '.', ''); // 1000.00
    }

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

}